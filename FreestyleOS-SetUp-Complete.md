# FreestyleOS UI Dev Getting Started

## Getting your development environment set up

The following commands are to be executed from the Terminal (MacOS) and they will install the base tools needed to develop and run the FreestyleOS.

1. Install Homebrew  
     ```bash
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
     ```
2. Install Java JDK 8
    ```bash
    brew tap caskroom/versions
    brew cask install java8
     ```
3. Install Maven
    ```bash
    brew install maven
    ```
4. Install NodeJS
    ```bash
    brew install node
    ```
5. Install git LFS
    ```bash
    brew install git-lfs
    ```
    *after installing follow the screen prompts to complete install.*
6. In the root directory of your machine create a `.m2` directory
    ```bash
    mkdir ~/.m2
    ```
7. In the recently created `.m2` directory create a `settings.xml` file and copy the content in the [link](https://bitbucket.org/snippets/kofreestyle/6eggK4/maven-settings-file).

## Setting up Core FreestyleOS

1. Clone the FOS_Core repository
    [https://bitbucket.org/kofreestyle/fos_core](https://bitbucket.org/kofreestyle/fos_core)
2. Create a `start.sh` file in the root of the project containing the contents of
    [https://bitbucket.org/snippets/kofreestyle/derr6K](https://bitbucket.org/snippets/kofreestyle/derr6K)

    **Do not commit this file to the repo**
3. **Build the Project:** for the initial setup and to rebuild after pulling updates, enter the following in the command line (from project root)
    ```bash
    mvn -DskipTests clean install
    ```
4. **Start the Dev Server:** to start the development server, run the `start.sh` file that was created during setup
    - check that in the `start.sh`  file the target snapshot version matches the one that was recently built.
    - `. start.sh` or `source start.sh`

## Setting up the simulator

1. Clone the FOS_Applications repository
    [https://bitbucket.org/kofreestyle/fos_deliverables](https://bitbucket.org/kofreestyle/fos_deliverables)
2. In the repository navigate to `./simulator`
3. Use Maven to build the appropriate simulator and branding combination using the `mvm clean package` command.
    ```bash
    # to build wih the FHS brand set and for the 9100 platform
    mvn clean package -P9100 -PFHS

    # to build with the uc-us brand set and for the 7000 platform
    mvn clean package -P7000 -Puc-us

    # to build with the uc-eu brand set and for the 3100 platform
    mvn clean package -P3100 -Puc-eu

    # to build with the xpoc-default brand set
    mvn clean package -P9100 -Pxpoc-default

    # to build with the powerade-default brand set
    mvn clean package -P9100 -Ppowerade-default
    ```
    *In order to know what profiles are available they can be found on the `pom.xml` file starting on line 68*
4. Once it is finished building, there will be a `./target` folder inside of the `./simulator` folder of your `fos_deliverables` repository.
5. Copy that "**fos**" folder to the root location of your `C:/` drive
6. In the **fos** folder on your terminal navigate to `./release/bin`
7. Should be a `run.bat` file or a `run.sh` file.  
    - **Windows** => Double-click on the `run.bat` file to start the simulator  
    - Mac OSX => In the terminal run `. run.sh` or `source run.sh`

You can use this address to view the **NCUI / CUI** of your simulator in your web browser of choice: [http://localhost:8080/app/ncui/index.html](http://localhost:8080/app/ncui/index.html) or [http://localhost:8080/app/index.html](http://localhost:8080/app/index.html)

### Important Parts of the Simulator

1. The "fos" folder : the heart of the simulator.  You'll be grabbing the zip of this every time you rebuild it.  I would recommend deleting the entire "fos" folder and replacing it with the new one in your C:/ drive.
2. The "release" folder inside the "fos" folder : The guts.  This will be where your pkgs and run files are.  More on that later.
3. The "data" folder inside the "fos" folder : This is the brain.  Essentially, this is the database.  Delete the entire "data" folder if you want to completely wipe the DBs and have a clean dispenser state.
4. The "bin" folder inside the "release" folder : The run files.  You use these to run the simulator.  You will notice a "model" flag in the file if you edit it via notepad(++).  You can change that to change the model (i.e. XPOC, 9100, 7000, 3100)
5. The "content" folder inside the "release" folder : The content pkgs.  These indicate your ingredient data, brand set, beverage data, CUI, NCUI, etc.
6. The "testKeys" folder inside the "release" folder : The test keys.  These need to match to properly allow you to use your pkgs.  Generally, you don't have to worry about these, but if you suddenly find your content pkgs deleted-- these may lead to the cause.


## Setting up NCUI FreestyleOS

**NCUI** *Non-Consumer User Interface* The dispenser user interface used by technicians and crew

1. Clone the FOS_Applications repository
    [https://bitbucket.org/kofreestyle/fos_applications/](https://bitbucket.org/kofreestyle/fos_applications/)
2. In the terminal navigate to the `./default/ncui` directory
3. run npm install
    ```bash
    npm install
    ```
4. Now you are ready to run the Development Server
    ```bash
    npm start
    ```

## Setting up CUI FreestyleOS

**CUI** *Consumer User Interface* The user interface that consumers use to operate a dispenser

1. In the fOS_applications repository navigate to `./default/cui` directory
2. run npm install
    ```bash
    npm install
    ```
3. Now you are ready to run the Development Server
    ```bash
    npm start
    ```

## Setting up Swagger docs

The following steps are to create the documentation for the available APIs

1. In the fOS_applications repository navigate to `./extras/swagger-docs`
2. Open the `pom.xml` file
3. copy the ID in the profiles section
    ```xml
    <!-- Profiles -->
    <profiles>
        <profile>
            <id>extrasNodeSupport</id>
    ```
4. On the terminal navigate to `./extras/swagger-docs` and use the following command with the id after -P
    ```bash
    mvn clean install -PextrasNodeSupport
    ```
5. At this step we are are going to add a basic node [http-server](https://www.npmjs.com/package/http-server)
    ```bash
    sudo npm install http-server -g
    ```
6. In the terminal navigate to `./target/swagger`
7. Initiate a node or python server in any other port besides 3000
    ```bash
    http-server -p 8889
    ```
    OR
    ```bash
    python -m SimpleHTTPServer 8889
    ```
8. On the browser navigate  to [http://127.0.0.1:2500](http://127.0.0.1:8889) *(node)* OR [http://0.0.0.0:2500](http://0.0.0.0:8889) *(python)*