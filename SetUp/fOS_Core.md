# Setting up Core FreestyleOS

1. Clone the FOS_Core repository
    [https://bitbucket.org/kofreestyle/fos_core](https://bitbucket.org/kofreestyle/fos_core)
2. Create a `start.sh` file in the root of the project containing the contents of
    [https://bitbucket.org/snippets/kofreestyle/derr6K](https://bitbucket.org/snippets/kofreestyle/derr6K)

    **Do not commit this file to the repo**
3. **Build the Project:** for the initial setup and to rebuild after pulling updates, enter the following in the command line (from project root)
    ```bash
    mvn -DskipTests clean install
    ```
4. **Start the Dev Server:** to start the development server, run the `start.sh` file that was created during setup
    - check that in the `start.sh`  file the target snapshot version matches the one that was recently built.
    - `. start.sh` or `source start.sh`