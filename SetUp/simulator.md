# Setting up the simulator

1. Clone the FOS_Applications repository [https://bitbucket.org/kofreestyle/fos_deliverables](https://bitbucket.org/kofreestyle/fos_deliverables)
2. In the repository navigate to `./simulator`
3. Use Maven to build the appropriate simulator and branding combination using the `mvm clean package` command.

    ```bash
        # to build wih the FHS brand set and for the 9100 platform
        mvn clean package -P9100 -PFHS

        # to build with the uc-us brand set and for the 7000 platform
        mvn clean package -P7000 -Puc-us

        # to build with the uc-eu brand set and for the 3100 platform
        mvn clean package -P3100 -Puc-eu

        # to build with the xpoc-default brand set
        mvn clean package -P9100 -Pxpoc-default

        # to build with the powerade-default brand set
        mvn clean package -P9100 -Ppowerade-default
    ```

    *In order to know what profiles are available they can be found on the `pom.xml` file starting on line 68*

4. Once it is finished building, there will be a `./target` folder inside of the `./simulator` folder of your `fos_deliverables` repository.
5. Copy that "**fos**" folder to the root location of your `C:/` drive
6. In the **fos** folder on your terminal navigate to `./release/bin`
7. Should be a `run.bat` file or a `run.sh` file. 
    - **Windows** => Double-click on the `run.bat` file to start the simulator  
    - Mac OSX => In the terminal run `. run.sh` or `source run.sh`

You can use this address to view the **NCUI / CUI** of your simulator in your web browser of choice: [http://localhost:8080/app/ncui/index.html](http://localhost:8080/app/ncui/index.html) or [http://localhost:8080/app/index.html](http://localhost:8080/app/index.html)

## Important Parts of the Simulator

1. The "fos" folder : the heart of the simulator. You'll be grabbing the zip of this every time you rebuild it. I would recommend deleting the entire "fos" folder and replacing it with the new one in your C:/ drive.
2. The "release" folder inside the "fos" folder : The guts. This will be where your pkgs and run files are. More on that later.
3. The "data" folder inside the "fos" folder : This is the brain. Essentially, this is the database. Delete the entire "data" folder if you want to completely wipe the DBs and have a clean dispenser state.
4. The "bin" folder inside the "release" folder : The run files. You use these to run the simulator. You will notice a "model" flag in the file if you edit it via notepad(++). You can change that to change the model (i.e. XPOC, 9100, 7000, 3100)
5. The "content" folder inside the "release" folder : The content pkgs. These indicate your ingredient data, brand set, beverage data, CUI, NCUI, etc.
6. The "testKeys" folder inside the "release" folder : The test keys. These need to match to properly allow you to use your pkgs. Generally, you don't have to worry about these, but if you suddenly find your content pkgs deleted-- these may lead to the cause.
