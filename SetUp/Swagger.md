# Setting up Swagger docs

The following steps are to create the documentation for the available APIs

1. In the fOS_applications repository navigate to `./extras/swagger-docs`
2. Open the `pom.xml` file
3. copy the ID in the profiles section
    ```xml
    <!-- Profiles -->
    <profiles>
        <profile>
            <id>extrasNodeSupport</id>
    ```
4. On the terminal navigate to `./extras/swagger-docs` and use the following command with the id after -P
    ```bash
    mvn clean install -PextrasNodeSupport
    ```
5. At this step we are are going to add a basic node [http-server](https://www.npmjs.com/package/http-server)
    ```bash
    sudo npm install http-server -g
    ```
6. In the terminal navigate to `./target/swagger`
7. Initiate a node or python server in any other port besides 3000
    ```bash
    http-server -p 8889
    ```
    OR
    ```bash
    python -m SimpleHTTPServer 8889
    ```
8. On the browser navigate  to [http://127.0.0.1:2500](http://127.0.0.1:8889) *(node)* OR [http://0.0.0.0:2500](http://0.0.0.0:8889) *(python)*