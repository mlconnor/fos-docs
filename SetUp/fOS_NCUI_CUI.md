# Setting up NCUI FreestyleOS

**NCUI** *Non-Consumer User Interface* The dispenser user interface used by technicians and crew

1. Clone the FOS_Applications repository
    [https://bitbucket.org/kofreestyle/fos_applications/](https://bitbucket.org/kofreestyle/fos_applications/)
2. check out the `develop` branch
    ```bash
    git checkout develop
    ```
3. In the terminal navigate to the `./default/ncui` directory
4. run npm install
    ```bash
    npm install
    ```
5. Now you are ready to run the Development Server
    ```bash
    npm start
    ```

# Setting up CUI FreestyleOS

**CUI** *Consumer User Interface* The user interface that consumers use to operate a dispenser

1. In the fOS_applications repository navigate to `./default/cui` directory
2. check out the `develop` branch
    ```bash
    git checkout develop
    ```
3. run npm install
    ```bash
    npm install
    ```
4. Now you are ready to run the Development Server
    ```bash
    npm start
    ```
