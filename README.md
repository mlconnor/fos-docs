# FreestyleOS Development documentation

This Repository is meant to be a repository of the processes used in the Freestyle software group for development.

## Getting your development environment prerequisites

The following commands are to be executed from the Terminal (MacOS) and they will install the base tools needed to develop and run the FreestyleOS.

1. Install Homebrew  
     ```bash
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
     ```
2. Install Java JDK 8
    ```bash
    brew tap caskroom/versions
    brew cask install java8
     ```
3. Install Maven
    ```bash
    brew install maven
    ```
4. Install NodeJS
    ```bash
    brew install node
    ```
5. Install git LFS
    ```bash
    brew install git-lfs
    ```
    *after installing follow the screen prompts to complete install.*
6. In the root directory of your machine create a `.m2` directory
    ```bash
    cd /
    mkdir .m2
    ```
7. In the recently created `.m2` directory create a `settings.xml` file and copy the content in the [link](https://bitbucket.org/snippets/kofreestyle/6eggK4/maven-settings-file).

## For UI development (minimum)

1. Set up the [Simulator](/SetUp/simulator.md)
2. Set up [NCUI / CUI](/SetUp/fOS_NCUI_CUI.md)

## Other fOS setups

- [Swagger Docs](/SetUp/Swagger.md)
- [fOS Core](/SetUp/fOS_Cotr.md)